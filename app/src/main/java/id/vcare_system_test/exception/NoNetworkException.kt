package id.vcare_system_test.exception

import java.io.IOException

/**
 * Custom exception class which will let the user if no internet available
 * */
open class NoNetworkException : IOException() {

    override fun getLocalizedMessage(): String? = "No Internet Connection"
}