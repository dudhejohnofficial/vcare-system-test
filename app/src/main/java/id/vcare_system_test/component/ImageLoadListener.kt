package id.vcare_system_test.component

/**
 * Created on 10/4/16.
 */
interface ImageLoadListener {
    fun onImageLoadCompleted(success: Boolean)
}