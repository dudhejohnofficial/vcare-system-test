package id.vcare_system_test.datasource

import com.google.gson.JsonObject
import id.vcare_system_test.response.FeedResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface ApiServices {

    @POST("index.php")
    fun getFeedList(
        @Query("p") type: String,
        @Body body: JsonObject?
    ): Call<FeedResponse>
}