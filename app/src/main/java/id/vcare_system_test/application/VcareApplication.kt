package id.vcare_system_test.application

import android.app.Application
import id.vcare_system_test.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.logger.Level

class VcareApplication : Application() {


    override fun onCreate() {
        super.onCreate()
        stopKoin()
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@VcareApplication)
            modules(listOf(
                viewModelModules,
                constantModules, useCaseModule, remoteDataSourceModule, repoModule
            ))
        }
    }
}