package id.vcare_system_test.model

/**
 * data class to hold the viewpager data
 * */
data class PagerFragmentDataModel(
    var pageTitle: Int?,
    var pageText: Int?,
    var pageTopImage: Int?,
    var pageMidImage: Int?,
    var pageBackground: Int?
)