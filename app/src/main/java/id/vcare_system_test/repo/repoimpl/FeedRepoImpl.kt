package id.vcare_system_test.repo.repoimpl

import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import id.vcare_system_test.datasource.ApiServices
import id.vcare_system_test.network.NetworkCall
import id.vcare_system_test.network.Resource
import id.vcare_system_test.repo.FeedRepo
import id.vcare_system_test.response.FeedResponse

class FeedRepoImpl(private val apiServices: ApiServices) : FeedRepo {

    private val registerCall = NetworkCall<FeedResponse>()

    override suspend fun getFeeds(jsonObject: JsonObject): MutableLiveData<Resource<FeedResponse>> =
        registerCall.makeCall(apiServices.getFeedList("showAllVideos",jsonObject))
}