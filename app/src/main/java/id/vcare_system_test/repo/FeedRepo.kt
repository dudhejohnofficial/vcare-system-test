package id.vcare_system_test.repo

import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import id.vcare_system_test.network.Resource
import id.vcare_system_test.response.FeedResponse

interface FeedRepo {


    /**
     * used to get the Feeds
     * */
    suspend fun getFeeds(
        jsonObject: JsonObject
    ): MutableLiveData<Resource<FeedResponse>>
}