package id.vcare_system_test.viewModel

import androidx.lifecycle.*
import id.vcare_system_test.exception.ApiFailureException
import id.vcare_system_test.network.Resource
import id.vcare_system_test.network.Status
import id.vcare_system_test.response.FeedResponse
import id.vcare_system_test.usecase.FeedUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FeedViewModel(private val feedUseCase: FeedUseCase) : ViewModel() {

    private val _validationError = MutableLiveData<String>()
    val validationError: LiveData<String> = _validationError

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _response = MutableLiveData<FeedResponse>()
    val feedResponse: LiveData<FeedResponse> = _response

    private val _apiError = MutableLiveData<ApiFailureException>()
    val apiError: LiveData<ApiFailureException> = _apiError

    init {
        viewModelScope.launch(Dispatchers.Main) {
            feedUseCase.getFeedList().observeForever {
                responseObserver.onChanged(it)
            }
        }
    }

    private val responseObserver: Observer<Resource<FeedResponse>> =
        Observer { t -> processResponse(t) }



    private fun processResponse(response: Resource<FeedResponse>?) {

        when (response?.status) {
            Status.SUCCESS -> {
                _isLoading.value = false
                _response.value = response.data
            }

            Status.ERROR -> {
                _apiError.value = response.apiError
                _isLoading.value = false
            }

            Status.LOADING -> {
                _isLoading.value = true
            }
        }
    }
}