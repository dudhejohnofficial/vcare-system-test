package id.vcare_system_test.usecase

import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import id.vcare_system_test.network.Resource
import id.vcare_system_test.repo.FeedRepo
import id.vcare_system_test.response.FeedResponse

class FeedUseCase(private val feedRepo: FeedRepo) {

    suspend fun getFeedList(): MutableLiveData<Resource<FeedResponse>> {
        return feedRepo.getFeeds(constructJson())
    }

    private fun constructJson(): JsonObject {
        val jsonObject = JsonObject()
        try {
            jsonObject.addProperty("fb_id", "0")
            jsonObject.addProperty("token", "token")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return jsonObject
    }
}