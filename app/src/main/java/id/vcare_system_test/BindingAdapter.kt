package id.vcare_system_test

import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import android.widget.RadioGroup
import android.widget.RatingBar
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import id.vcare_system_test.component.CircleImageView
import id.vcare_system_test.component.VCaremageView

/**
 * Binding adapter class for binding the adapter image
 * */
object BindingAdapter {

    /**
     * To show image from url
     * @param imageView
     * @param imageUrl
     * @param placeholder
     */
    @JvmStatic
    @BindingAdapter(value = ["android:src", "placeholder"], requireAll = false)
    fun setImageUrl(view: ImageView, profileUrl: String?, placeHolder: Drawable?) {

        Glide.with(view.context)
            .load(profileUrl)
            .placeholder(placeHolder)
            .error(placeHolder)
            .into(view)
    }

    /**
     * To show image from drawable
     *
     * @param imageView
     * @param drawableId
     * @param placeholder
     */
    @JvmStatic
    @BindingAdapter(value = ["drawableId", "placeholder"], requireAll = false)
    fun setDrawableImage(view: ImageView, drawableId: Int, placeHolder: Drawable?) {
        Glide.with(view.context)
            .load(if (drawableId != 0) drawableId else null)
            .placeholder(placeHolder)
            .error(placeHolder)
            .into(view)
    }

    /**
     * to set the rating for the rating view
     * */
    @JvmStatic
    @BindingAdapter("rating")
    fun setRating(view: RatingBar?, rating: String?) {
        if (view != null && rating != null) {
            val rate = rating.toFloat()
            view.rating = rate
        }
    }

    /**
     * Sets the view as visible if true otherwise as invisible
     */
    @JvmStatic
    @BindingAdapter("loadImage")
    fun loadImage(view: VCaremageView, url: String?) {
        if (!TextUtils.isEmpty(url)) {
            view.loadImageWithUrl(url)
        }
    }

    /**
     * Sets the view as visible if true otherwise as invisible
     */
    @JvmStatic
    @BindingAdapter("loadCircularImage")
    fun loadImage(view: CircleImageView, url: String?) {
        view.loadImageWithRelativeUrl(
            url,
            R.drawable.v_profile_img_place_holder,
            null
        )
    }

    /**
     * Sets the view as visible if true otherwise as invisible
     */
    @JvmStatic
    @BindingAdapter("visibleOrGone")
    fun visibleOrGone(view: View, visible: Boolean) {
        view.visibility = if (visible) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }
}
