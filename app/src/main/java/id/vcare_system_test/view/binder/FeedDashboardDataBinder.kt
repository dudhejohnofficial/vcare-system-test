package id.vcare_system_test.view.binder

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import id.vcare_system_test.response.Msg

/**
 * Binder class to bind the UI data
 * */
class FeedDashboardDataBinder {

    private var childData = MutableLiveData<Msg>()

    val profielPic: LiveData<String> = MediatorLiveData<String>().apply {
        addSource(childData) {
            value = it.user_info.profile_pic
        }
    }

    val name: LiveData<String> = MediatorLiveData<String>().apply {
        addSource(childData) {
            value = it.user_info.first_name +  " " + it.user_info.last_name
        }
    }

    val date: LiveData<String> = MediatorLiveData<String>().apply {
        addSource(childData) {
            value = "Today 1:45 PM"
        }
    }

    val feedContent: LiveData<String> = MediatorLiveData<String>().apply {
        addSource(childData) {
            value = it.thum
        }
    }

    val description: LiveData<String> = MediatorLiveData<String>().apply {
        addSource(childData) {
            value = it.description
        }
    }

    /**
     * to bind each item inside the row on dashboard item
     */
    @MainThread
    fun childBind(response: Msg) {
        childData.value = response
    }
}