package id.vcare_system_test.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.vcare_system_test.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}