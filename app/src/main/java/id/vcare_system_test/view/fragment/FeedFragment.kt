package id.vcare_system_test.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import id.vcare_system_test.R
import id.vcare_system_test.databinding.FeedFragmentBinding
import id.vcare_system_test.view.adapter.FeedListAdapter
import id.vcare_system_test.viewModel.FeedViewModel
import org.koin.android.viewmodel.scope.getViewModel

class FeedFragment : BaseFragment() {

    companion object {
        fun newInstance() = FeedFragment()
    }

    private lateinit var binding : FeedFragmentBinding

    private val adapter = FeedListAdapter(this)

    private val viewModel by lazy {
        viewModelScope?.getViewModel<FeedViewModel>(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.feed_fragment,
            container,
            false
        )
        binding.lifecycleOwner = this
        binding.rvChildRow.adapter = adapter
        binding.viewModel = viewModel

        return binding.root
    }

    override fun setActionListener() {
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel?.feedResponse?.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it.msg)
        })

        viewModel?.apiError?.observe(viewLifecycleOwner, Observer {

        })

        viewModel?.validationError?.observe(viewLifecycleOwner, Observer {


        })
    }

}