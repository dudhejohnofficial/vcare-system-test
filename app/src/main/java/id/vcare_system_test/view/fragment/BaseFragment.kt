package id.vcare_system_test.view.fragment

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import id.vcare_system_test.R
import id.vcare_system_test.di.VCARE_SCOPE_ID
import id.vcare_system_test.di.VCARE_SCOPE_QUALIFIER
import id.vcare_system_test.exception.ApiFailureException
import kotlinx.coroutines.*
import org.koin.core.KoinComponent
import org.koin.core.scope.Scope
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

/**
 * Login base fragment
 * */
abstract class BaseFragment(
    private val parentContext: CoroutineContext = EmptyCoroutineContext
) :
    Fragment(), KoinComponent {

    protected lateinit var coroutineScope: CoroutineScope
        private set
    val viewModelScope: Scope? get() = _currentScope
//    private val progressDialog = ProgressDialog.getInstance(false)
//    private var errorBottomSheetFragment: ErrorViewBottomSheetFragment? = null

    /**
     * abstract function to write the all the click listener
     * */
    abstract fun setActionListener()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        coroutineScope =
            CoroutineScope(Dispatchers.Main + parentContext + SupervisorJob(parentContext[Job]))
    }

    override fun onResume() {
        super.onResume()
        checkLocationEnabled()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionListener()
    }

    private fun checkLocationEnabled() {
        val lm = context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        var gpsEnabled = false
        var networkEnabled = false

        try {
            gpsEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (e: Exception) {
            e.printStackTrace()
        }

        try {
            networkEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (!gpsEnabled && !networkEnabled) {
            AlertDialog.Builder(context)
                .setTitle("Alert")
                .setMessage("Please enable your location, Press ok to go settings")
                .setCancelable(false)
                .setPositiveButton("OK") { _, _ ->
                    requireActivity().startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))

                }.show()
        }
    }

    override fun onDestroy() {
        coroutineScope.cancel()
        super.onDestroy()
    }

    private fun internalCreateScope(): Scope? {
        val created = getKoin().getScopeOrNull(VCARE_SCOPE_ID) == null
        val scope = getKoin()
            .getOrCreateScope(VCARE_SCOPE_ID, VCARE_SCOPE_QUALIFIER)

        if (created) {
            scope.declare(this)
        }

        return scope
    }

    private var _currentScope = internalCreateScope()

    /**
     * Destroy/close out a logged in scope. Should only be called when a user is logging out
     */
    fun destroyScope() {
        _currentScope.also { scope ->
            _currentScope = null
            scope?.close()
        }
    }

    /**
     * Start a new logged in scope.  If one is already active then it is returned without creation
     */
    fun startcope(): Scope? = internalCreateScope().apply {
        _currentScope = this
    }

    protected fun showErrorMessage(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    protected fun showError(message: String, view: View) {
        val snack = Snackbar.make(
            view,
            message,
            Snackbar.LENGTH_INDEFINITE
        )
        snack.setAction(resources.getString(R.string.ok)) {
            snack.dismiss()
        }
        snack.show()
    }

    protected fun showProgress(show: Boolean) {
     /*   if (show) {
            progressDialog.show(childFragmentManager, "Progress")
        } else {
            progressDialog.dismissDialog()
        }*/
    }

    protected fun showErrorView(callback: () -> Unit?, exception: ApiFailureException) {

        try {
            showProgress(false)

            /*if (exception.code == INVALID_TOKEN) {
                navigateToLogin()
            } else {
                errorBottomSheetFragment = ErrorViewBottomSheetFragment.newInstance(callback, exception)
                errorBottomSheetFragment?.show(childFragmentManager, "dialog")
            }*/
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    protected fun dismissErrorView() {
//        errorBottomSheetFragment?.dismiss()
    }

    private fun navigateToLogin() {
        Toast.makeText(
            requireContext(),
            "User session has been expired, Please Login again",
            Toast.LENGTH_SHORT
        ).show()
        requireActivity().finish()
    }
}