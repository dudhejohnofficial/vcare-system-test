package id.vcare_system_test.view.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager
import id.vcare_system_test.R
import id.vcare_system_test.databinding.FragmentWorkFlowBinding
import id.vcare_system_test.model.PagerFragmentDataModel
import id.vcare_system_test.view.activity.MainActivity
import id.vcare_system_test.view.adapter.SliderPagerAdapter

/**
 * A simple [Fragment] subclass.
 * Use the [WorkFlowFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class WorkFlowFragment : BaseFragment() {
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var binding: FragmentWorkFlowBinding

    private lateinit var adapter: SliderPagerAdapter
    override fun setActionListener() {

        binding.btnLogin.setOnClickListener {
            requireActivity().finish()
            requireActivity().startActivity(Intent(requireContext(), MainActivity::class.java))
        }

        binding.btnSignUp.setOnClickListener {
            requireActivity().finish()
            requireActivity().startActivity(Intent(requireContext(), MainActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.fragment_work_flow,
            container,
            false
        )
        binding.lifecycleOwner = this

        setUpAdapter()

        return binding.root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment WorkFlowFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            WorkFlowFragment()
    }


    private fun formFragmentData(): ArrayList<PagerFragmentDataModel> {
        val fragmentDataModel = ArrayList<PagerFragmentDataModel>()
        fragmentDataModel.add(
            PagerFragmentDataModel(
                null,
                R.string.dummy_description,
                null,
                null,
                null
            )
        )
        fragmentDataModel.add(
            PagerFragmentDataModel(
                null,
                R.string.dummy_description,
                null,
                null,
                null
            )
        )
        fragmentDataModel.add(
            PagerFragmentDataModel(
                null,
                R.string.dummy_description,
                null,
                null,
                null
            )
        )
        return fragmentDataModel
    }

    private fun setUpAdapter() {
        adapter = SliderPagerAdapter(
            childFragmentManager, formFragmentData()
        )
        binding.pagerIntroSlider.adapter = adapter
        binding.tabs.setupWithViewPager(binding.pagerIntroSlider)

        binding.pagerIntroSlider.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                if (position == adapter.count - 1) {

                }
            }
        })
    }
}