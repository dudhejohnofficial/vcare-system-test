package id.vcare_system_test.view.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import id.vcare_system_test.model.PagerFragmentDataModel
import id.vcare_system_test.view.fragment.SliderFragment

/**
 * This adapter used to show the list of waiting and completed appointment with the pager
 * */
class SliderPagerAdapter(
    supportFragmentManager: FragmentManager,
    private val formFragmentData: ArrayList<PagerFragmentDataModel>
) :
    FragmentStatePagerAdapter(supportFragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment =
        SliderFragment.newInstance()

    override fun getCount(): Int =
        formFragmentData.size
}
