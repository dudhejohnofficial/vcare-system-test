package id.vcare_system_test.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.MainThread
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import id.vcare_system_test.databinding.DashboardListItemBinding
import id.vcare_system_test.response.Msg
import id.vcare_system_test.view.binder.FeedDashboardDataBinder

/**
 * Adapter class to show list of doctors
 * */
class FeedListAdapter(
    private val lifecycleOwner: LifecycleOwner
) : ListAdapter<Msg, FeedListAdapter.DashboardListViewHolder>(
    SEARCH_COMPARATOR
) {
    private var itemList: List<Msg> = emptyList()
    private var mainPos: Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedListAdapter.DashboardListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = DashboardListItemBinding.inflate(layoutInflater, parent, false)
        itemBinding.lifecycleOwner = lifecycleOwner
        return DashboardListViewHolder(
            itemBinding
        )
    }

    companion object {
        private val SEARCH_COMPARATOR = object : DiffUtil.ItemCallback<Msg>() {
            override fun areItemsTheSame(
                oldItem: Msg,
                newItem: Msg
            ): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: Msg,
                newItem: Msg
            ): Boolean =
                oldItem == newItem
        }
    }

    override fun onBindViewHolder(holder: FeedListAdapter.DashboardListViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    /**
     * Holder class for Dashboard list adapter
     * */
    inner class DashboardListViewHolder(
        private val itemBinding: DashboardListItemBinding
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        /**
         * bind function to bind the doctor details in each list
         * */
        @MainThread
        fun bind(dashboardChildItem: Msg) {
            val binder = FeedDashboardDataBinder()
            binder.childBind(dashboardChildItem)
            itemBinding.binder = binder
            itemBinding.executePendingBindings()
        }
    }
}
