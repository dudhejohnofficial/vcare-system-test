package id.vcare_system_test.network

/**
 * An enum class which holds the key for network status
 * */
enum class Status {
        SUCCESS,
        ERROR,
        LOADING
}
