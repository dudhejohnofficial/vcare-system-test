package id.vcare_system_test.di

import org.koin.core.qualifier.named
import org.koin.dsl.module

/**
 * ScopeID for the current logged in scope
 */
const val VCARE_SCOPE_ID = "MY_CART_SCOPE_ID"

/**
 * Qualifier for the logged in scope
 */
val VCARE_SCOPE_QUALIFIER = named(VCARE_SCOPE_ID)

val constantModules = module {
//    single { PreferenceManager(androidContext()) }
}