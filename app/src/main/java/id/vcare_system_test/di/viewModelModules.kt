package id.vcare_system_test.di

import id.vcare_system_test.repo.FeedRepo
import id.vcare_system_test.repo.repoimpl.FeedRepoImpl
import id.vcare_system_test.usecase.FeedUseCase
import id.vcare_system_test.viewModel.FeedViewModel
import org.koin.dsl.module
import org.koin.androidx.viewmodel.dsl.viewModel

val viewModelModules = module {

    scope(VCARE_SCOPE_QUALIFIER) {

        viewModel { FeedViewModel(get()) }
    }
}

val useCaseModule = module {

    scope(VCARE_SCOPE_QUALIFIER) {
        scoped {
            FeedUseCase(get())
        }
    }
}

val repoModule = module {

    scope(VCARE_SCOPE_QUALIFIER) {
        scoped<FeedRepo> {
            FeedRepoImpl(get())
        }
    }
}
