package id.vcare_system_test.response

data class FeedResponse(
    val code: String,
    val msg: List<Msg>
)

data class Msg(
    val count: Count,
    val created: String,
    val description: String,
    val fb_id: String,
    val gif: String,
    val id: String,
    val liked: String,
    val sound: Sound,
    val thum: String,
    val user_info: UserInfo,
    val video: String
)

data class Count(
    val like_count: String,
    val video_comment_count: String
)

data class Sound(
    val audio_path: AudioPath,
    val created: Any,
    val description: Any,
    val id: Any,
    val section: Any,
    val sound_name: Any,
    val thum: Any
)

data class UserInfo(
    val first_name: String,
    val last_name: String,
    val profile_pic: String,
    val username: String,
    val verified: String
)

data class AudioPath(
    val acc: String,
    val mp3: String
)